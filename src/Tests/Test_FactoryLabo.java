package Tests;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import TP.Affichage;
import TP.FactoryLabo;
import TP.GestionLaboratoire;
import TP.Visiteur;

class Test_FactoryLabo {
	
	FactoryLabo labo = new FactoryLabo();
	GestionLaboratoire facade;
	
	@Test
	void testReadXML() {
		try {
			
			facade = labo.ReadXML();
			Visiteur visiteur = new Affichage();
			facade.applique(visiteur);
			
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	void testVerifBureau1() {
		//num�ros diff�rents car bureaux cr��s avec BureauFactory
		assertTrue(labo.VerifNumBureaux());
	}
	
	void testVerifBureau2() {
		//num�ros diff�rents car bureaux cr��s avec BureauFactory
		assertFalse(labo.VerifNumBureaux());
	}
}
