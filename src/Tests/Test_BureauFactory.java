package Tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import org.junit.jupiter.api.Test;

import TP.Bureau;
import TP.BureauFactory;
import TP.ConnexionReseau;
import TP.Equipement;
import TP.Ordinateur;

class Test_BureauFactory {

	BureauFactory factoryLenovo = new BureauFactory("Lenovo", 420, 69);

	@Test
	void testBureauConstructor() {
		
		Bureau bureauLenovo = factoryLenovo.BureauConstructor();
		
		List<Equipement> equipements = bureauLenovo.GetEquipements();
		
		Equipement pc = equipements.get(0);
		assertTrue(pc instanceof Ordinateur);
		Ordinateur pc1 = (Ordinateur)pc;
		assertEquals(pc1.GetPuissance(), 420);
		assertTrue(pc1.GetMarque().equals("Lenovo"));
		
		Equipement lan = equipements.get(1);
		assertTrue(lan instanceof ConnexionReseau);
		ConnexionReseau lan1 = (ConnexionReseau)lan;
		assertEquals(lan1.GetNumero(), 69);
		
		assertEquals(bureauLenovo.GetNumero(), 0);
	}
	
	@Test
	void testNbBureau() {
		
		@SuppressWarnings("unused")
		Bureau bureauLenovo1 = factoryLenovo.BureauConstructor();
		@SuppressWarnings("unused")
		Bureau bureauLenovo2 = factoryLenovo.BureauConstructor();
		
		assertEquals(factoryLenovo.GetNbBureaux(),2);
	}
}
