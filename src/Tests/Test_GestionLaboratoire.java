package Tests;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import TP.Bureau;
import TP.Categorie;
import TP.ConnexionReseau;
import TP.DAO;
import TP.Equipement;
import TP.FactoryLabo;
import TP.Ordinateur;
import TP.Personne;
import TP.Verification;

class Test_GestionLaboratoire {
	
	Personne personneInvalide = new Personne("toto2", Categorie.stagiaire);
	Personne personneValide = new Personne("tEst", Categorie.postDoc);
	
	Bureau bureauInvalide;
	Bureau bureauValide;
	
	List<Equipement> equipementsInvalides = new ArrayList<Equipement>();
	
	DAO facadeDAO;
	FactoryLabo factory = new FactoryLabo();

	@BeforeEach
	void BeforeAll() throws SAXException, IOException, ParserConfigurationException {
		
		facadeDAO = factory.ReadXML();
		
		bureauValide = new Bureau(1, null);
		
		equipementsInvalides.add(new Ordinateur("Test", 1));
		equipementsInvalides.add(new ConnexionReseau(1));
		equipementsInvalides.add(new ConnexionReseau(2));
		equipementsInvalides.add(new ConnexionReseau(3));
		bureauInvalide = new Bureau(99, equipementsInvalides);
	}
	
	@Test
	void testBureaux() {
		//test bureaux
		Verification verif = new Verification();
		assertFalse(verif.VerifBureau(bureauInvalide));
		assertTrue(verif.VerifBureau(bureauValide));
	}
	
	@Test
	void testAjouterBureau1() {
		//test bureau existant
		assertEquals(facadeDAO.AjouterBureau(facadeDAO.GetBureaux().get(0)), -1);
	}
	
	@Test
	void testAjouterBureau2() {
		//test bureau invalide
		assertEquals(facadeDAO.AjouterBureau(bureauInvalide), 0);
	}
	
	@Test
	void testAjouterBureau3() {
		//test succ�s
		assertEquals(facadeDAO.AjouterBureau(bureauValide), 1);
		assertTrue(facadeDAO.GetBureaux().get((facadeDAO.GetBureaux().size())-1).equals(bureauValide));
	}
	
	@Test
	void testSupprimerBureau1() {
		//bureau inexistant
		assertFalse(facadeDAO.SupprimerBureau(bureauInvalide));
	}
	
	@Test
	void testSupprimerBureau2() {
		//succes
		Bureau check = facadeDAO.GetBureaux().get(0);
		assertTrue(facadeDAO.SupprimerBureau(check));
		assertFalse(facadeDAO.GetBureaux().contains(check));
	}
	
	@Test
	void testModifierBureau1() {
		//test bureau invalide
		assertEquals(facadeDAO.ModifierBureau(facadeDAO.GetBureaux().get(0), bureauInvalide), 0);
	}
	
	@Test
	void testModifierBureau2() {
		//test bureau inexistant
		assertEquals(facadeDAO.ModifierBureau(bureauInvalide, bureauValide), -1);
	}
	
	@Test
	void testModifierBureau3() {
		//test succ�s
		bureauValide.SetPersonne(new Personne("Test", Categorie.stagiaire));
		assertEquals(facadeDAO.ModifierBureau(facadeDAO.GetBureaux().get(0), bureauValide), 1);
		assertTrue(facadeDAO.GetBureaux().get(0).equals(bureauValide));
	}
	
	@Test
	void testAjouterPersonne1() {
		//test personne d�j� dans la liste
		assertEquals(facadeDAO.AjouterPersonne(facadeDAO.GetPersonnes().get(0)), -1);
	}
	
	@Test
	void testAjouterPersonne2() {
		//test nom de la personne invalide
		assertEquals(facadeDAO.AjouterPersonne(personneInvalide), 0);
	}
	
	@Test
	void testAjouterPersonne3() {
		//test succ�s
		assertEquals(facadeDAO.AjouterPersonne(personneValide), 1);
		assertTrue(facadeDAO.GetPersonnes().get((facadeDAO.GetPersonnes().size())-1).equals(personneValide));
	}
	
	@Test
	void testSupprimerPersonne1() {
		//test personne inexistante
		assertFalse(facadeDAO.SupprimerPersonne(personneInvalide));
	}
	
	@Test
	void testSupprimerPersonne2() {
		//test succ�s
		Personne check = facadeDAO.GetPersonnes().get(0);
		assertTrue(facadeDAO.SupprimerPersonne(check));
		assertFalse(facadeDAO.GetPersonnes().contains(check));
		for (Bureau bureau : facadeDAO.GetBureaux()) {
			if(bureau.GetPersonne() != null) {
				assertFalse(bureau.GetPersonne().equals(check));
			}
        }
	}
	
	@Test
	void testModifierPersonne1() {
		//test personne inexistante
		assertEquals(facadeDAO.ModifierPersonne(personneInvalide, personneValide), -1);
	}
	
	@Test
	void testModifierPersonne2() {
		//test personne nom non valide
		assertEquals(facadeDAO.ModifierPersonne(facadeDAO.GetPersonnes().get(0), personneInvalide), 0);
	}
	
	@Test
	void testModifierPersonne3() {
		//test succ�s
		assertEquals(facadeDAO.ModifierPersonne(facadeDAO.GetPersonnes().get(0), personneValide), 1);
		assertTrue(facadeDAO.GetPersonnes().get(0).equals(personneValide));
		//check facade.GetBureaux()
	}
	
	@Test
	void testAjouterEquipement1() {
		//test bureau inexistant
		assertEquals(facadeDAO.AjouterEquipement(bureauInvalide, null), -1);
	}
	
	@Test
	void testAjouterEquipement2() {
		//test liste �quipement invalide
		assertEquals(facadeDAO.AjouterEquipement(facadeDAO.GetBureaux().get(0), bureauInvalide.GetEquipements()), 0);
	}
	
	@Test
	void testAjouterEquipement3() {
		//test succ�s
		List<Equipement> equipementsValides = new ArrayList<Equipement>();
		equipementsValides.add(new Ordinateur("Test", 1));
		equipementsValides.add(new ConnexionReseau(1));
		assertEquals(facadeDAO.AjouterEquipement(facadeDAO.GetBureaux().get(0), equipementsValides), 1);
		assertTrue(facadeDAO.GetBureaux().get(0).GetEquipements().equals(equipementsValides));
	}
	
	@Test
	void testSupprimerEquipement1() {
		//test bureau inexistant
		assertEquals(facadeDAO.SupprimerEquipement(bureauInvalide, bureauInvalide.GetEquipements().get(0)), -1);
	}
	
	@Test
	void testSupprimerEquipement2() {
		//test equipement inexistant
		assertEquals(facadeDAO.SupprimerEquipement(facadeDAO.GetBureaux().get(0), new Ordinateur("jenexistepas", 66)), -2);
	}
	
	@Test
	void testSupprimerEquipement3() {
		//test succ�s
		Equipement check = facadeDAO.GetBureaux().get(0).GetEquipements().get(0);
		assertEquals(facadeDAO.SupprimerEquipement(facadeDAO.GetBureaux().get(0), check), 1);
		assertFalse(facadeDAO.GetBureaux().get(0).GetEquipements().contains(check));
	}
	
	@Test
	void testModifierEquipement1() {
		//test bureau inexistant
		assertEquals(facadeDAO.ModifierEquipement(bureauInvalide, bureauInvalide.GetEquipements().get(0), bureauInvalide.GetEquipements().get(0)), -1);
	}
	
	@Test
	void testModifierEquipement2() {
		//test equipement inexistant
		assertEquals(facadeDAO.ModifierEquipement(facadeDAO.GetBureaux().get(0), new Ordinateur("jenexistepas", 66), new Ordinateur("test", 66)), -2);
	}
	
	@Test
	void testModifierEquipement3() {
		//test succ�s
		Equipement check = new Ordinateur("test", 66);
		assertEquals(facadeDAO.ModifierEquipement(facadeDAO.GetBureaux().get(0), facadeDAO.GetBureaux().get(0).GetEquipements().get(0), check), 1);
		assertTrue(facadeDAO.GetBureaux().get(0).GetEquipements().get(0).equals(check));
	}
}
