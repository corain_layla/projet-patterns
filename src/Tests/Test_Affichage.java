package Tests;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import TP.FactoryLabo;
import TP.GestionLaboratoire;
import TP.Visiteur;
import TP.Affichage;

class Test_Affichage {
	
	FactoryLabo factory = new FactoryLabo();

	@Test
	void test() throws SAXException, IOException, ParserConfigurationException {

		GestionLaboratoire facade = factory.ReadXML();
		Visiteur visiteur = new Affichage();
		facade.applique(visiteur);
	}
}
