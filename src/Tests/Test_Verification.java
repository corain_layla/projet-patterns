package Tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import TP.Bureau;
import TP.ConnexionReseau;
import TP.Equipement;
import TP.Ordinateur;
import TP.Personne;
import TP.Verification;

class Test_Verification {
	
	List<Equipement> equipements = new ArrayList<Equipement>();
	Bureau bureau = new Bureau(1, equipements);
	Verification verificateur = new Verification();
	
	@Test
	void testVerifBureau1() {
		//Bureau valide
		
		equipements.add(new Ordinateur("Dell", 100));
		equipements.add(new Ordinateur("HP", 100));
		
		equipements.add(new ConnexionReseau(0));
		equipements.add(new ConnexionReseau(0));
		equipements.add(new ConnexionReseau(1));
		
		bureau.SetEquipements(equipements);
		assertTrue(verificateur.VerifBureau(bureau));
	}
	
	void testVerifBureau2() {
		//trop de ConnexionReseau
		
		equipements.add(new ConnexionReseau(2));
		
		bureau.SetEquipements(equipements);
		assertFalse(verificateur.VerifBureau(bureau));
	}
	
	void testVerifBureau3() {
		//trop d'ordinateurs
		
		equipements.remove(equipements.size()-1);
		
		equipements.add(new Ordinateur("Lenovo", 100));
		equipements.add(new Ordinateur("msi", 100));
		
		bureau.SetEquipements(equipements);
		assertFalse(verificateur.VerifBureau(bureau));
	}
	
	void testVerifBureau4() {
		//trop de ConnexionReseau ET trop d'Ordinateur
		
		equipements.add(new ConnexionReseau(2));
		
		bureau.SetEquipements(equipements);
		assertFalse(verificateur.VerifBureau(bureau));
	}
	
	Personne personne = new Personne("teSt", null);
	
	@Test
	void testVerifNom1() {
		//test nom valide
		
		assertTrue(verificateur.VerifNom(personne));
		assertTrue(personne.GetNom().equals("Test"));
	}
	
	@Test
	void testVerifNom2() {
		//test nom avec numeros
		
		personne.SetNom("teSt1");
		assertFalse(verificateur.VerifNom(personne));
		assertTrue(personne.GetNom().equals("teSt1"));
	}
}
