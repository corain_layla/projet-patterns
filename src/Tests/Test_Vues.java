package Tests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import TP.Bureau;
import TP.Categorie;
import TP.FactoryLabo;
import TP.GestionLaboratoire;
import TP.LaboController;
import TP.Personne;
import TP.VueBureau;
import TP.VueListeBureaux;

class Test_Vues {

	GestionLaboratoire facade;
	LaboController controleur;
	
	List<Bureau> bureaux = new ArrayList<Bureau>();
	List<Personne> personnes = new ArrayList<Personne>();
	FactoryLabo labo = new FactoryLabo();

	@BeforeEach
	void BeforeAll() throws SAXException, IOException, ParserConfigurationException {

		facade = labo.ReadXML();
		controleur = new LaboController(facade);
	}
	
	@Test
	void testVueListeBureaux() {
		
		VueListeBureaux vueListeBureaux = controleur.CreerVueListeBureau();
		
		System.out.println("vueListeBureaux");
		System.out.println("AFFICHAGE 1");
		vueListeBureaux.Update();
		System.out.println("AFFICHAGE 2");
		assertTrue(controleur.SupprimerBureau(facade.GetBureaux().get(3)));
		System.out.println("AFFICHAGE 3");
		assertEquals(controleur.AjouterBureau(new Bureau(99, null)), 1);
	}

	@Test
	void testVueBureau() {
		
		VueBureau vueBureau = controleur.CreerVueBureau(facade.GetBureaux().get(0));
		
		System.out.println("vueBureaux");
		System.out.println("AFFICHAGE 1");
		vueBureau.Update();
		System.out.println("AFFICHAGE 2");
		Bureau newBureau = facade.GetBureaux().get(0);
		newBureau.SetNumero(99);
		newBureau.SetPersonne(new Personne("Test", Categorie.stagiaire));
		assertEquals(controleur.ModifierBureau(newBureau), 1);
	}
}
