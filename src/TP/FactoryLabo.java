package TP;

import java.util.ArrayList;
import java.util.Iterator;
import java.io.IOException;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException; 

public class FactoryLabo {
	
	private static final String FILENAME = "C:\\Users\\corai\\Nextcloud\\Documents\\Cours\\M1\\IDL_2\\file.xml";
	
	private List<Bureau> bureaux = new ArrayList<Bureau>();
	private List<Personne> personnes = new ArrayList<Personne>();
	
	private Verification verificator = new Verification();
	
	public FactoryLabo () {}

	/**Construit une facade GestionLaboratoire � partir d'un fichier XML contenant une liste de bureaux et leurs attributs.
	 * 
	 * @return Retourne une facade compl�te
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public GestionLaboratoire ReadXML() throws SAXException, IOException, ParserConfigurationException {
		
		DocumentBuilderFactory usine = DocumentBuilderFactory.newDefaultInstance();
	    DocumentBuilder fabrique = usine.newDocumentBuilder();
	    Document doc = fabrique.parse(FILENAME);
	
	    NodeList bureauNoeudList = doc.getElementsByTagName("Bureau");
	
	    for (int indexBur = 0; indexBur < bureauNoeudList.getLength(); indexBur++) {
			Node noeudBur = bureauNoeudList.item(indexBur);
			
			if (noeudBur.getNodeType() == Node.ELEMENT_NODE) {
				Element bureauElement = (Element) noeudBur;
				
				int numeroBur = Integer.parseInt(bureauElement.getAttribute("numero"));
				Bureau monBureau = new Bureau(numeroBur, null);
				
				monBureau.SetEquipements(CreationEquipement(bureauElement));
				monBureau.SetPersonne(CreationPersonne(bureauElement));
				   
				if (verificator.VerifBureau(monBureau)) {
					bureaux.add(monBureau);
				}   
			}
	    }  
		return new GestionLaboratoire(bureaux, personnes);
	}
	
	/**
	 * 
	 * @param elem
	 * @return Liste d'�quipement d'un bureau
	 */
	private List<Equipement> CreationEquipement(Element elem) {
		   
	    int nbOrdinateur = elem.getElementsByTagName("Ordinateur").getLength();
	    int nbConnexion = elem.getElementsByTagName("ConnexionReseau").getLength();
	   
	    List<Equipement> equipements = new ArrayList<Equipement>();
	   
	    for (int index = 0; index < nbOrdinateur; index++) {
	      Node noeudOrdi = elem.getElementsByTagName("Ordinateur").item(index);
	      equipements.add(CreationOrdinateur(noeudOrdi));
	    }
	   
	    for (int index = 0; index < nbConnexion; index++) {
	      Node noeudConnex = elem.getElementsByTagName("ConnexionReseau").item(index);
	      equipements.add(CreationConnexion(noeudConnex));
	    }
	   
	    return equipements;
	}
	
	/**
	 * 
	 * @param noeud
	 * @return Retourne un �quipement Ordinateur
	 */
	private Ordinateur CreationOrdinateur(Node noeud) {
		
	    Element elem = (Element) noeud;
	   
	    String marque = elem.getAttribute("marque");
	    int puissance = Integer.parseInt(elem.getAttribute("puissance"));
	   
	    return new Ordinateur(marque, puissance);
	}
	
	/**
	 * 
	 * @param noeud
	 * @return Retourne un �quipement ConnexionReseau
	 */
	private ConnexionReseau CreationConnexion(Node noeud) {
		
	    Element elem = (Element) noeud;
	    int numero = Integer.parseInt(elem.getAttribute("numero"));
	   
	    return new ConnexionReseau(numero);
	}
	
	/**Retourne la personne lue dans le XML et l'ajoute � la liste des personnes au passage
	 * 
	 * @param elem
	 * @return Personne
	 */
	private Personne CreationPersonne(Element elem) {
		   
	    if (elem.getElementsByTagName("Personne").getLength() == 0) {
	      return null;
	    }
	   
	    Node noeud = elem.getElementsByTagName("Personne").item(0);
	   
	    Element elemPersonne = (Element) noeud;
	   
	    String nom = elemPersonne.getAttribute("nom");
	    String nomCategorie = elemPersonne.getAttribute("Categorie");
	   
	    Categorie categorie;
	   
	    switch (nomCategorie) {
	      case "stagiaire":
	        categorie = Categorie.stagiaire;
	        break;
	      case "postDoc":
	        categorie = Categorie.postDoc;
	        break;
	      case "thesard":
	      default:
	        categorie = Categorie.thesard;
	    }
	   
	    Personne applicant = new Personne(nom, categorie);
	   
	    if (verificator.VerifNom(applicant)) {
	    	personnes.add(applicant);
	    	return applicant;
	    }
	   
	    return null;
	}
	
	/**
	 * V�rifie que tous les Bureau ont des numeros diff�rents
	 * @return true si c'est le cas, sinon retourne false
	 */
	public boolean VerifNumBureaux() {
		
		List<Integer> listeNumeros = new ArrayList<Integer>();
		
		Iterator<Bureau> iter = bureaux.iterator();
		
		while (iter.hasNext()) {
			
			Bureau bureau = iter.next();
			
			if (listeNumeros.contains(bureau.GetNumero())) {
	    		return false;
	    	} else {
	    		listeNumeros.add(bureau.GetNumero());
	    	}
		}
		return true;
	}
	
	// Getters & Setters
	public List<Bureau> GetBureaux() {
		return bureaux;
	}
	
	public void SetBureaux(List<Bureau> newBureaux) {
		bureaux = newBureaux;
	}
	
	public List<Personne> GetPersonnes() {
		return personnes;
	}
	
	public void SetPersonnes(List<Personne> newPersonnes) {
		personnes = newPersonnes;
	}
}