package TP;

public class Ordinateur extends Equipement{

	private String marque;
	private int puissance;
	
	public Ordinateur (String newMarque, int newPuissance) {
		marque = newMarque;
		puissance = newPuissance;
	}
	
	//Getters & Setters
	
	public String GetMarque() {
		return marque;
	}
	
	public void SetMarque(String newMarque) {
		marque = newMarque;
	}
	
	public int GetPuissance() {
		return puissance;
	}
	
	public void SetPuissance(int newPuissance) {
		puissance = newPuissance;
	}

	@Override
	public void applique(Visiteur unVisiteur) {
		unVisiteur. agitSur(this);
	}
}
