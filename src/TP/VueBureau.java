package TP;

public class VueBureau extends Vue {

	private Bureau bureau;
	
	public VueBureau(Bureau newBureau, GestionLaboratoire facade) {
		bureau = newBureau;
		facade.AbonnerVue(this);
	}
	
	/**
	 * Rafraichit l'affichage de la vue: Un bureau avec ses d�tails
	 */
	@Override
	public void Update() {
		
		Visiteur visiteur = new Affichage();
		bureau.applique(visiteur);
		bureau.GetPersonne().applique(visiteur);
    	for (Equipement equipement : bureau.GetEquipements()) {
    		equipement.applique(visiteur);
    	}
	}
}
