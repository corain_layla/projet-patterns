package TP;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class VueListeBureaux extends Vue {

	private List<Bureau> bureaux = new ArrayList<Bureau>();
	
	public VueListeBureaux (List<Bureau> newBureaux, GestionLaboratoire facade) {
		bureaux = newBureaux;
		facade.AbonnerVue(this);
	}
	
	/**
	 * Met � jour l'affichage : Liste des num�ros de bureaux.
	 */
	@Override
	public void Update() {
		
		Visiteur visiteur = new Affichage();
		Iterator<Bureau> iter = bureaux.iterator();
	    while (iter.hasNext()) {
	    	Bureau b = iter.next();
	    	b.applique(visiteur);
	    	System.out.println("--------------------------");
	    }
	}
}
