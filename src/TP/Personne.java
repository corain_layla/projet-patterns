package TP;

public class Personne implements Element{

	private String nom;
	private Categorie statut = Categorie.thesard;
	
	public Personne (String newNom, Categorie newCat) {
		nom = newNom;
		
		if (newCat != null) {
			statut = newCat;
		}
	}
	
	//Getters & Setters

	public String GetNom() {
		return nom;
	}

	public void SetNom(String newNom) {
		nom = newNom;
	}

	public Categorie GetStatut() {
		return statut;
	}

	public void SetStatut(Categorie newCat) {
		statut = newCat;
	}

	@Override
	public void applique(Visiteur unVisiteur) {
		unVisiteur. agitSur(this);
	}
}