package TP;

public class ConnexionReseau extends Equipement{
	
	private int numero;
	
	public ConnexionReseau (int newNum) {
		numero = newNum;
	}

	//Getters & Setters
	
	public int GetNumero() {
		return numero;
	}

	public void SetNumero(int newNum) {
		numero = newNum;
	}

	@Override
	public void applique(Visiteur unVisiteur) {
		unVisiteur. agitSur(this);
	}
}
