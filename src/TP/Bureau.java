package TP;

import java.util.ArrayList;
import java.util.List;

public class Bureau implements Element {

		private int numero;
		private List<Equipement> equipements = new ArrayList<Equipement>();
		private Personne personne;
		
		public Bureau (int newNum, List<Equipement> liste) {
			numero = newNum;
			equipements = liste;
			personne = null;
		}

		//Getters & Setters
		
		public int GetNumero() {
			return numero;
		}

		public void SetNumero(int newNum) {
			numero = newNum;
		}

		public List<Equipement> GetEquipements() {
			return equipements;
		}

		public void SetEquipements(List<Equipement> liste) {
			equipements = liste;
		}

		public Personne GetPersonne() {
			return personne;
		}

		public void SetPersonne(Personne newPersonne) {
			personne = newPersonne;
		}

		@Override
		public void applique(Visiteur unVisiteur) {
			unVisiteur. agitSur(this);
		}
}
