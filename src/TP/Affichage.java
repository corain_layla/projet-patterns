package TP;

import java.util.Iterator;
import java.util.List;

public class Affichage implements Visiteur {
	
	public void agitSur(Bureau bureau) {
		System.out.println("Bureau");
		System.out.println("Num�ro: "+ bureau.GetNumero());
	}
	
	public void agitSur(Personne personne) {
		System.out.println("Occupant");
		System.out.println("Nom : "+ personne.GetNom());
		System.out.println("Statut : "+ personne.GetStatut());
	}
	
	public void agitSur(Equipement equipement) {
		if (equipement instanceof ConnexionReseau) {
			System.out.println("Connexion r�seau");
			System.out.println("Num�ro : "+ ((ConnexionReseau) equipement).GetNumero());
		} else if (equipement instanceof Ordinateur) {
			System.out.println("Ordinateur");
			System.out.println("Marque : "+ ((Ordinateur) equipement).GetMarque());
			System.out.println("Puissance : "+ ((Ordinateur) equipement).GetPuissance());
		}
	}
	
	public void agitSur (GestionLaboratoire facade) {
		System.out.println("Laboratoire :");
		List<Bureau> bureaux = facade.GetBureaux();
		
		Iterator<Bureau> iter = bureaux.iterator();
	    while (iter.hasNext()) {
	    	System.out.println("--------------------------");
	    	Bureau b = iter.next();
	    	b.applique(this);
	    	if(b.GetPersonne() != null) {
	    		b.GetPersonne().applique(this);
	    	}
	    	if(b.GetEquipements() != null) {
		    	for (Equipement equipement : b.GetEquipements()) {
		    		equipement.applique(this);
		    	}
	    	}
	    }
		System.out.println("fin");
	}
}