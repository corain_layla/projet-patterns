package TP;

public interface Visiteur {
	
	void agitSur(Bureau bureau);
	void agitSur(Personne personne);
	void agitSur(Equipement equipement);
	void agitSur(GestionLaboratoire facade);
}
