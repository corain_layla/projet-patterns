package TP;

import java.util.ArrayList;
import java.util.List;

public class BureauFactory {
	
	private int nb = 0;
	
	private String marque;
	private int puissance;
	private int lan;
	
	public BureauFactory (String newMarque, int newPuissance, int newNumLan) {
		
		marque = newMarque;
		puissance = newPuissance;
		lan = newNumLan;
	}
	
	public Bureau BureauConstructor() {
		
		List<Equipement> list = new ArrayList<Equipement>();
		
		list.add(new Ordinateur(marque, puissance));
		list.add(new ConnexionReseau(lan));
		
		Bureau b = new Bureau (nb, list);
		nb++;
		
		return b;
	}
	
	//Getters & Setters

	public String GetMarque() {
		return marque;
	}

	public void SetMarque(String newMarque) {
		marque = newMarque;
	}

	public int GetPuissance() {
		return puissance;
	}

	public void SetPuissance(int newPuissance) {
		puissance = newPuissance;
	}

	public int GetNumLan() {
		return lan;
	}

	public void SetNumLan(int newNumLan) {
		lan = newNumLan;
	}
	
	public int GetNbBureaux() {
		return nb;
	}
}
