package TP;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Verification {
	
	public Verification () {}

	/**
	 * V�rifie qu�un Bureau poss�de au plus 2 ConnexionReseau, dont les numeros sont diff�rents, et au plus 3 Ordinateur
	 * @param bureau Bureau � v�rifier
	 * @return true si TOUTES les conditions d�crites si dessus sont v�rifi�es, sinon false
	 */
	public boolean VerifBureau (Bureau bureau) {
		
		int countLan = 0;
		int countPc = 0;
		
		List<Integer> numerosLan = new ArrayList<Integer>();
		
		List<Equipement> list = new ArrayList<Equipement>();
		list = bureau.GetEquipements();
		
		if (list != null) {
			Iterator<Equipement> iter = list.iterator();
			
		    while (iter.hasNext()) {
		    	
		    	Equipement e = iter.next();
		    	
		    	if(e instanceof ConnexionReseau) {
		    		if (! numerosLan.contains(((ConnexionReseau) e).GetNumero())) {
		    			countLan++;
		    			numerosLan.add(((ConnexionReseau) e).GetNumero());
		    		}
		    	} else if (e instanceof Ordinateur) {
	    			countPc++;
		    	}
		    }
		}
	    
		if ((countLan <= 2) && (countPc <= 3)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * V�rifie que le nom d�une Personne ne contient que des lettres et si c�est le cas,
	 * modifiez la casse pour que les lettres soient toutes en minuscule sauf la premi�re en majuscule.
	 * @param p Personne dont le nom est � v�rifier
	 * @return retourne false si le nom contient des chiffres, true sinon
	 */
	public boolean VerifNom(Personne p) {
		
		String nom = p.GetNom();
		
		if (nom.matches("[a-zA-Z]+")) {
			
			nom = nom.toLowerCase();
			nom = nom.substring(0, 1).toUpperCase() + nom.substring(1);
			p.SetNom(nom);
			
			return true;
			
		} else {
			return false;
		}
	}
}