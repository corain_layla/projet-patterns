package TP;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GestionLaboratoire implements Element, DAO {
	
	private List<Bureau> bureaux = new ArrayList<Bureau>();
	private List<Personne> personnes = new ArrayList<Personne>();
	private List<Vue> vues = new ArrayList<Vue>();
	
	private Verification verificator = new Verification();
	
	public GestionLaboratoire(List<Bureau> newBureaux, List<Personne> newPersonnes) {
		bureaux = newBureaux;
		personnes = newPersonnes;
	}
	
	public void AbonnerVue(Vue vue) {
		vues.add((Vue)vue);
	}
	
	public void SupprimerVue(Vue vue) {
		vues.remove((Vue)vue);
	}
	
	private void notifierVues() {
		 for (Vue vue : vues) {
            vue.Update();
        }
	}
	
	//GESTION CRUD
	//Bureau
	
	/**Ajoute un bureau � la liste de bureaux
	 * 
	 * @param newBureau Bureau � ajouter
	 * @return -1 si le bureau indiqu� existe d�j�; 0 si le bureau est invalide; 1 si succ�s
	 */
	@Override
	public int AjouterBureau(Bureau newBureau) {
		
		if(bureaux.contains(newBureau)) {
			return -1;	//bureau existe d�j�
		}
		
		if (!verificator.VerifBureau(newBureau)) {
			return 0; //bureau invalide
		}
		
		//succ�s
		bureaux.add(newBureau);
		notifierVues();
		return 1;
	}
	
	/**Supprime un bureau de la liste de bureaux
	 * 
	 * @param bureau Bureau � supprimer
	 * @return True si l'op�ration a r�ussi
	 */
	@Override
	public boolean SupprimerBureau(Bureau bureau) {
		
		if(bureaux.remove(bureau)) {
    		notifierVues();
    		return true;
		} else {
			return false;
		}
	}
	
	/**Modifie un bureau en le remplacant un par un nouveau
	 * 
	 * @param oldBureau Bureau � remplacer
	 * @param newBureau Nouveau bureau
	 * @return 0 si bureau invalide; -1 is bureau inexistant; 1 si succ�s
	 */
	@Override
	public int ModifierBureau(Bureau oldBureau, Bureau newBureau) {
		
		if (!verificator.VerifBureau(newBureau)) {
			//bureau invalide
			return 0;
		}
		
		Iterator<Bureau> iter = bureaux.iterator();
		int i = 0;
	    while (iter.hasNext()) {
	    	
	    	Bureau bureau = iter.next();
	    	
	    	if(bureau.equals(oldBureau)) {
	    		//succ�s
	    		bureaux.set(i, newBureau);
	    		notifierVues();
	    		return 1;
	    	}
	    	i++;
	    }
	    
	    //bureau inexistant
	    return -1;
	}

	//Personne
	
	/**Ajoute une personne � la liste de personnes
	 * 
	 * @param newPersonne Personne � ajouter
	 * @return -1 si la personne existe d�j�; 0 si le nom de la personne est invalide; 1 si succ�s
	 */
	@Override
	public int AjouterPersonne(Personne newPersonne) {
		
		if(personnes.contains(newPersonne)) {
			return -1;	//personne existe d�j�
		}
		
		if (!verificator.VerifNom(newPersonne)) {
			return 0; //personne invalide
		}
		
		//succ�s
		notifierVues();
		personnes.add(newPersonne);
		return 1;
	}
	
	/**Supprime une personne de la liste des personnes et de la liste des bureaux
	 * 
	 * @param personne Personne � supprimer
	 * @return true si l'op�ration a r�ussi; false si la personne n'existe pas
	 */
	@Override
	public boolean SupprimerPersonne(Personne personne) {
		
		//maj liste personne
		if (personnes.remove(personne)) {
	    	
			Iterator<Bureau> iter = bureaux.iterator();
			int i = 0;
		    while (iter.hasNext()) {
		    	
		    	Bureau bureau = iter.next();
		    	
		    	if((bureau.GetPersonne() != null) && (bureau.GetPersonne().equals(personne))) {
		    		//maj liste bureaux
		    		bureaux.get(i).SetPersonne(null);
		    	}
		    	i++;
		    }
		    //succ�s
		    notifierVues();
			return true;
	    	
	    } else {
		    //personne inexistante
		    return false;
	    }
	}
	
	/**Modifie une personne dans la liste des personnes et des bureaux en la rempla�ant par une nouvelle
	 * 
	 * @param oldPersonne Personne � �craser
	 * @param newPersonne Nouvelle personne
	 * @return 0 newPersonne invalide; -1 oldPersonne inexistante; 0 succ�s
	 */
	@Override
	public int ModifierPersonne(Personne oldPersonne, Personne newPersonne) {
		
		if (!verificator.VerifNom(newPersonne)) {
			//personne invalide
			return 0;
		}
		
		Iterator<Personne> iter = personnes.iterator();
		int i = 0;
	    while (iter.hasNext()) {
	    	
	    	Personne personne = iter.next();
	    	
	    	if(personne.equals(oldPersonne)) {
	    		
	    		//maj liste personne
	    		personnes.set(i, newPersonne);
	    		
	    		Iterator<Bureau> iter1 = bureaux.iterator();
	    		
	    		int j = 0;
	    	    while (iter1.hasNext()) {
	    	    	Bureau bureau = iter1.next();
	    	    	
	    	    	if( (bureau.GetPersonne() != null) && (bureau.GetPersonne().equals(personne))) {
	    	    		//maj liste bureaux
	    	    		bureaux.get(j).SetPersonne(newPersonne);
	    	    	}
	    	    	j++;
	    	    }
	    	    //succ�s
	    	    notifierVues();
	    		return 1;
	    	}
	    	
	    	i++;
	    }
	    //personne inexistante
	    return -1;
	}
	
	//Equipement
	
	/**Modifie la liste d'�quipement d'un bureau en la rempla�ant par une nouvelle
	 * 
	 * @param bureau Bureau � modifier
	 * @param newEquipement Nouvelle liste d'�quipement
	 * @return 0 si la liste d'equipement est invalide; -1 si le bureau est inexistant; 1 si succ�s
	 */
	@Override
	public int AjouterEquipement(Bureau bureau, List<Equipement> newEquipement) {
		
		Iterator<Bureau> iter = bureaux.iterator();
		int i = 0;
	    while (iter.hasNext()) {
	    	
	    	Bureau b = iter.next();
	    	
	    	if(b.equals(bureau)) {
	    		
	    		List<Equipement> list = b.GetEquipements();
	    		bureaux.get(i).SetEquipements(newEquipement);
	    		
	    		if(!verificator.VerifBureau(bureaux.get(i))) {
	    			
	    			//bureau invalide apr�s maj
	    			bureaux.get(i).SetEquipements(list);
	    			return 0;
	    			
	    		} else {
	    			//succes
		    		notifierVues();
		    		return 1;
	    		}
	    	}
	    	i++;
	    }
	    
	    //bureau inexistant
	    return -1;
	}
	
	/**Supprime un �quipement de la liste d'�quipements d'un bureau donn�
	 * 
	 * @param bureau Bureau dont l'�quipement est � supprimer
	 * @param equipement Equipement � supprimer
	 * @return -1 si bureau inexistant; -2 si equipement inexistant; 1 si succ�s
	 */
	@Override
	public int SupprimerEquipement(Bureau bureau, Equipement equipement) {
		
		Iterator<Bureau> iter = bureaux.iterator();
		int i = 0;
	    while (iter.hasNext()) {
	    	
	    	Bureau b = iter.next();
	    	
	    	if(b.equals(bureau)) {
	    		
	    		List<Equipement> list = b.GetEquipements();
	    		if(!list.remove(equipement)) {
	    			//equipement inexistant
	    			return -2;
	    		}
	    		
	    		//succes
	    		bureaux.get(i).SetEquipements(list);
	    		notifierVues();
	    		return 1;
	    	}
	    	
	    	i++;
	    }
	    
	    //bureau inexistant
	    return -1;
	}
	
	/**Modifie un �quipement de la liste d'�quipements d'un bureau donn�
	 * 
	 * @param bureau Bureau dont l'�quipement est � modifier
	 * @param equipement Equipement � supprimer
	 * @return -1 si bureau inexistant; -2 si equipement inexistant; 1 si succ�s
	 */
	@Override
	public int ModifierEquipement(Bureau bureau, Equipement equipement, Equipement newEquipement) {
		
		Iterator<Bureau> iter = bureaux.iterator();
		int i = 0;
	    while (iter.hasNext()) {
	    	
	    	Bureau b = iter.next();
	    	
	    	if(b.equals(bureau)) {
	    		
	    		List<Equipement> list = b.GetEquipements();
	    		
	    		Iterator<Equipement> iter1 = list.iterator();
	    		int j = 0;
	    	    while (iter1.hasNext()) {
	    	    	Equipement e = iter1.next();
	    	    	
	    	    	if (e.equals(equipement)) {
	    	    		list.set(j, newEquipement);
	    	    		Bureau newBureau = b;
	    	    		b.SetEquipements(list);
	    	    		bureaux.set(i, newBureau);
	    	    		
	    	    		//succes
	    	    		notifierVues();
	    	    		return 1;
	    	    	}
	    	    	
	    	    	j++;
	    	    }
	    	    
	    		//equipement inexistant
	    		return -2;
	    	}
	    	
	    	i++;
	    }
	    
	    //bureau inexistant
	    return -1;
	}

	@Override
	public void applique(Visiteur unVisiteur) {
		unVisiteur. agitSur(this);
	}
	
	//Getters
	@Override
	public List<Bureau> GetBureaux() {
		return bureaux;
	}

	public List<Personne> GetPersonnes() {
		return personnes;
	}
}
