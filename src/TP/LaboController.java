package TP;

public class LaboController {
	
	private DAO facadeDAO;
	private GestionLaboratoire facade;
	
	public LaboController(GestionLaboratoire labo) {
		facade = labo;
		facadeDAO = labo;
	}

	//Gestion des vues
	
	public VueBureau CreerVueBureau(Bureau bureau) {
		return new VueBureau(bureau, facade);
	}
	
	public VueListeBureaux CreerVueListeBureau() {
		return new VueListeBureaux(facade.GetBureaux(), facade);
	}
	
	public void SupprimerVue(Vue vue) {
		facade.SupprimerVue(vue);
	}
	
	//m�thodes appel�es par un bouton cliqu�
	
	public int ModifierBureau(Bureau bureau) {
		return facadeDAO.ModifierBureau(facadeDAO.GetBureaux().get(0), bureau);
	}
	
	public int AjouterBureau(Bureau bureau) {
		return facadeDAO.AjouterBureau(bureau);
	}
	
	public boolean SupprimerBureau(Bureau bureau) {
		return facadeDAO.SupprimerBureau(bureau);
	}
}