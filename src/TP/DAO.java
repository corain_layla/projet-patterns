package TP;

import java.util.List;

public interface DAO {
	
	public int AjouterBureau(Bureau newBureau);
	public boolean SupprimerBureau(Bureau bureau);
	public int ModifierBureau(Bureau oldBureau, Bureau newBureau);
	public int AjouterPersonne(Personne newPersonne);
	public boolean SupprimerPersonne(Personne personne);
	public int ModifierPersonne(Personne oldPersonne, Personne newPersonne);
	public int AjouterEquipement(Bureau bureau, List<Equipement> newEquipement);
	public int SupprimerEquipement(Bureau bureau, Equipement equipement);
	public int ModifierEquipement(Bureau bureau, Equipement equipement, Equipement newEquipement);
	public List<Bureau> GetBureaux();
	public List<Personne> GetPersonnes();
}
